from bs4 import BeautifulSoup
import requests
import time
import datetime
# import smtplib

import csv
import pandas as pd

#table header/fields
header = ['Id', 'Title', 'Price', 'Item', 'Per/Count', 'Date']
# name of .csv file to make        w = write        Unicode Transformation Format - 8 bits
# makes a csv file and a table with the header information from above  | as f = as file
with open('/Users/lewey/Hack Reactor/Mod 1/week-6/amazon-film-prices/amazon-web-scraper/amazon-film-prices.csv', 'w', newline='', encoding='UTF8') as f:
    writer = csv.writer(f)
    writer.writerow(header)

# function that scrapes desired web page then gathers specified information from it
# takes paramater id just and URL which is the given url for the desired web scraping
def check_price(id, URL):
    # user agent needed to access the interwebs
    headers = {"User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/105.0.0.0 Safari/537.36", "Accept-Encoding":"gzip, deflate", "Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8", "DNT":"1","Connection":"close", "Upgrade-Insecure-Requests":"1"}

    # saves requests.get as variable page
    # uses Beautiful soup to pull all html from specified page and prettify it
    page = requests.get(URL, headers=headers)
    soup1 = BeautifulSoup(page.content, "html.parser")
    soup2 = BeautifulSoup(soup1.prettify(), "html.parser")
    # finds specified data [product title, price, price/peritem]
    title = soup2.find(id='productTitle').get_text()
    price = soup2.find(class_='a-offscreen').get_text()
    per_count = None

    # not all items sell as pairs, if it doesnt have this html class, then price per item is full price
    if (soup2.find(class_='a-size-mini a-color-base aok-align-center pricePerUnit')):
        per_count = soup2.find(class_='a-size-mini a-color-base aok-align-center pricePerUnit').get_text()
        per_count = per_count.strip()[30:-71]
    else:
        per_count = price
        per_count = per_count.strip()[:]
    # formats the scraped information
    title = title.strip()[:]
    price = price.strip()[:]

    # logic to print number of items
    item = float(price.strip()[1:]) / float(per_count.strip()[1:])

    # sets date as today
    today = datetime.date.today()

    # preps the data to be displayed in each row
    data = [id, title, price, item, per_count, today]

    # creates data row to specified .csv file | a+ can read, write, and create (position at end of line)
    with open('amazon-film-prices.csv', 'a+', newline='', encoding='UTF8') as f:
        writer = csv.writer(f)
        writer.writerow(data)

# uses pandas to read a comma seperated value file and turns it into a dataframe at this location
df = pd.read_csv(r'/Users/lewey/Hack Reactor/Mod 1/week-6/amazon-film-prices/amazon-web-scraper/amazon-film-prices.csv')
print(df)

# initialize a while loop that initializes "check_price" function with id and specified url
# sets frequency to pull information
while(True):
    check_price(0, 'https://www.amazon.com/Kodak-Ektar-Professional-Exposures-Negative/dp/B06WLHBBZF/ref=sr_1_1?crid=1C9IQWEDJ7MNY&keywords=kodak+Ektar+100&qid=1663656525&s=music&sprefix=kodak+ektar+100%2Cpopular%2C105&sr=1-1')
    check_price(1, 'https://www.amazon.com/Kodak-Professional-Portra-Negative-Exposures/dp/B01N3Y6H3H/ref=sr_1_5?keywords=kodak+portra&qid=1663659298&s=music&sr=1-5')
    check_price(2, 'https://www.amazon.com/Ilford-1574577-Black-White-Exposures/dp/B002IS0E76/ref=sr_1_2_mod_primary_new?crid=JR9DY01IM1OW&keywords=ilford+film&qid=1663659391&s=music&sbo=RZvfv%2F%2FHxDF%2BO5021pAnSA%3D%3D&sprefix=ilford+film%2Cpopular%2C98&sr=1-2')
    check_price(3, 'https://www.amazon.com/Cinestill-800Tungsten-Speed-Format-800120/dp/B06ZZ2T741/ref=sr_1_11?m=A3TA2Z8CGC6NDC&marketplaceID=ATVPDKIKX0DER&qid=1663866741&s=merchant-items&sr=1-11')
    check_price(4, 'https://www.amazon.com/Kodak-Tri-X-400TX-Professional-Exposures/dp/B01MRHXNEM/ref=sr_1_3_mod_primary_new?crid=119RHJ7TUMRA0&keywords=tri-x&qid=1663867812&sbo=RZvfv%2F%2FHxDF%2BO5021pAnSA%3D%3D&sprefix=tri-x%2Caps%2C208&sr=8-3')
    # suspends execution of the current thread for a given amount of time
    # 3600 equivalent to one hour, 1 equivalent to 1 second
    time.sleep(3600)


# when you add an item that doesnot have a per_count field,
# make the default field the full price

# save day to day csv files
# inform me when a price drops or increases


# turn into a web app
